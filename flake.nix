{
  description = "ARTIQ controller for HighFinesse wavemeter";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-21.11;
  inputs.sipyco.url = github:m-labs/sipyco;  
  inputs.sipyco.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, sipyco }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      deps = [ sipyco.packages.x86_64-linux.sipyco ];
    in rec {
      packages.x86_64-linux = {
        highfinesse-net = pkgs.python3Packages.buildPythonPackage {
          pname = "highfinesse-net";
          version = "0.2";
          src = self;
          propagatedBuildInputs = [ sipyco.packages.x86_64-linux.sipyco ];
        };
      };

      defaultPackage.x86_64-linux = packages.x86_64-linux.highfinesse-net;

      devShell.x86_64-linux = pkgs.mkShell {
        name = "highfinesse-net-dev-shell";
        buildInputs = [
          (pkgs.python3.withPackages(ps: [ packages.x86_64-linux.highfinesse-net ]))
        ];
      };
    };
}
